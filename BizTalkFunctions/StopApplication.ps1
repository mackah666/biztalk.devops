﻿
function stopApplication(
   [Parameter(Position=0,Mandatory=$true, HelpMessage="Application Name")]
   [string]$AppName,
   [Parameter(Position=1,Mandatory=$true, HelpMessage="Database Server Name")]
   [string]$SQLInstance,
   [Parameter(Position=2,Mandatory=$true, HelpMessage="Database Catalogue")]
   [string]$BizTalkManagementDb

)
{
 
    $BizTalkManagementDb ="BizTalkmgmtdb"
    [void] [System.reflection.Assembly]::LoadWithPartialName("Microsoft.BizTalk.ExplorerOM")
    $Catalog = New-Object Microsoft.BizTalk.ExplorerOM.BtsCatalogExplorer
    $Catalog.ConnectionString = "SERVER=$SQLInstance;DATABASE=$BizTalkManagementDb;Integrated Security=SSPI"
 
    $BTSApp = $Catalog.Applications[$AppName]

    #if($BTApp)
    #{
	    "Stopping " + $BTSApp.Name
        $BTSApp.Stop("StopAll")
        $catalog.SaveChanges()
    #}

}

        
