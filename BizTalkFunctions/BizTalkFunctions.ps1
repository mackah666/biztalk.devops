$BizTalkPath = (Get-ItemProperty  "hklm:SOFTWARE\Wow6432Node\Microsoft\Biztalk Server\3.0\").InstallPath
$FrameworkPath = (Get-ItemProperty  "hklm:SOFTWARE\Wow6432Node\Microsoft\.NETFramework\").InstallRoot

function ImportBTSResource
{
	param([string]$appName, [string]$resType, [string]$srcPath)
	$gacOptions="/Options:GacOnAdd,GacOnInstall"
	BTSTask.exe AddResource /Source:$srcPath /ApplicationName:$appName /Type:$resType /Overwrite $gacOptions
}

function GetMSBuildPath
{
    $MSBuildPath = Join-Path $FrameworkPath "v4.0.30319\MSBuild.exe"
    return $MSBuildPath
}


function InstallMSI{
    param([string]$msi,[string]$installPath)

    $log = $msi + ".log"

    $Arguments += "/i $msi"
    $Arguments += " /log $log"
    $Arguments += " /passive"
    $Arguments += " INSTALLDIR=`"$installPath`""

    Write-Host "Installing Msi with $Arguments." -ForegroundColor DarkGray;
    Write-Host ""

    Start-Process "msiexec.exe" $Arguments -Wait -Passthru
    #Write "Exitcode: $exitCode" -ForegroundColor DarkGray;
    Write ""

    #return $exitCode

}

#Function to uninstall MSI
function UninstallMSI{
    param([string]$msi)

    $app = Get-WmiObject -Class Win32_Product -Filter "PackageName = '$msi'"
    $app.Uninstall()
    
    Write-Host "Msi uninstalled." -ForegroundColor Green;

}

#Function to select specific environment settings file, there is no need to export it as BTDF exports it anyway
function ExportEnvironmentSettings
{
    param([string]$installPath, [string]$environment)
    
    $args = "`"" + (Join-Path $installPath "Deployment\EnvironmentSettings\SettingsFileGenerator.xml") + "`"" + " Deployment\EnvironmentSettings"
    $SettingsExporter = ("`"" + (Join-Path $installPath "\Deployment\Framework\DeployTools\EnvironmentSettingsExporter.exe") + "`"")
    
    Write-Host "SettingsExporter: $SettingsExporter" -ForegroundColor DarkGray;
    Write-Host "Arguments: $args" -ForegroundColor DarkGray;
    Write-Host ""
    
    Write-Host "Exporting Environment Settings file..." -ForegroundColor DarkGray;
    
    #$exitCode = (Start-Process -FilePath $SettingsExporter -ArgumentList $args -Wait -Passthru).ExitCode
    #if($exitCode -eq 0)
    #{
        $settingsFile = "Deployment\EnvironmentSettings\Exported_{0}Settings.xml" -f $environment
        $envSettingsFile = join-path $installPath $settingsFile
        
    #}
    
    Write-Host ""
    Write-Host "Env File: " $envSettingsFile -ForegroundColor DarkGray;
    
    return $envSettingsFile
    
    

}


#Function to deploy BTS application form BTDF deployment proj
function DeployApplication
{
    param([string]$installPath, [bool]$deployToMgmtDB,[string]$environment, [bool]$skipUnDeploy, [string]$deploymentProjFileName)

    if($environment -ne "NA")
    {
        $envSettingsFile = ExportEnvironmentSettings $installPath $environment
        
        Write-Host "ENV_SETTINGS " $envSettingsFile -ForegroundColor DarkGray;
        #Set-Item Env:ENV_SETTINGS -Value $envSettingsFile
        #Set-Item Env:BT_DEPLOY_MGMT_DB -Value $deployToMgmtDB
    }


	if($deploymentProjFileName -eq "")
	{
		$projFile = $installPath + "\Deployment\Deployment.btdfproj"
	}
	else
	{
		$projFile = $installPath + "\Deployment\" + $deploymentProjFileName
    }

	Write-Host "projFile: " $projFile -ForegroundColor DarkGray;

	$logfile = $installPath + "\DeployResults\DeployResults.txt"

    $Arguments += "`"$projFile`""
    $Arguments += " /p:DeployBizTalkMgmtDB=$deployToMgmtDB;Configuration=Server;SkipUndeploy=$skipUnDeploy;InstallDir=`"$installPath`""
    $Arguments += " /target:Deploy"
    $Arguments += " /l:FileLogger,Microsoft.Build.Engine;logfile=`"$logfile`""
    
    if($envSettingsFile -ne $null)
    {
        $Arguments += " /p:ENV_SETTINGS=`"$envSettingsFile`""
    }
    
    Write ""
    Write-Host $Arguments -ForegroundColor DarkGray;

    $MSBuildPath = GetMSBuildPath

    Write-Host "MSBuildPath: " $MSBuildPath -ForegroundColor DarkGray;
    Write-Host ""

    #Start-Process -FilePath $MSBuildPath -ArgumentList $Arguments -Wait -Passthru
    &$MSBuildPath $Arguments

}

#function to undeploy the biztalk project
function UndeployApplication
{
    param(
      [Parameter(Position=0,HelpMessage="Path wherein the app is installed")]
      [Alias("path")]
      [string]$ApplicationInstallPath,
      
      [Parameter(Position=1,HelpMessage="DeployBizTalkMgmtDB e.g. true if its first server to undeploy or false otherwise, optional by default true")]
      [Alias('firstserver')]
      [bool]$deployToMgmtDb = $true,
	  
	  [Parameter(Position=2,HelpMessage="Optionally, Proivde name of btdf deployment.proj file")]
	  [string]$deploymentProjFileName


      )
    $ErrorActionPreference = "Stop"
    
	if($deploymentProjFileName -eq "")
	{
		$projFile = $ApplicationInstallPath + "\Deployment\Deployment.btdfproj"
	}
	else
	{
		$projFile = $ApplicationInstallPath + "\Deployment\" + $deploymentProjFileName
		
    }
    $logfile = $ApplicationInstallPath + "\DeployResults\DeployResults.txt"

    $Arguments += "`"$projFile`""    
    $Arguments += " /p:DeployBizTalkMgmtDB=$deployToMgmtDB;Configuration=Server;InstallDir=`"$ApplicationInstallPath`""
    $Arguments += " /target:Undeploy"
    $Arguments += " /l:FileLogger,Microsoft.Build.Engine;logfile=`"$logfile`""
    
    Write ""
    Write-Host $Arguments -ForegroundColor DarkGray;

    $MSBuildPath = GetMSBuildPath

    Write-Host "MSBuildPath: " $MSBuildPath -ForegroundColor DarkGray;
    Write-Host ""    
    
    #Start-Process -FilePath $MSBuildPath -ArgumentList $Arguments -Wait -Passthru
    &$MSBuildPath $Arguments
    
    Write-Host "Undeployed finished" -ForegroundColor Green;
    
}

#function to do both install and deploy
function InstallAndDeployBizTalkApp
{
    param(
      [Parameter(Position=0,Mandatory=$true,HelpMessage="Msi file should be existing")]
      [ValidateScript({Test-Path $_})]
      [Alias("msi")]
      [string]$MsiFile,
       
      [Parameter(Position=1,Mandatory=$true,HelpMessage="Path wherein the resource file will be installed")]
      [Alias("path")]
      [string]$ApplicationInstallPath,
       
      [Parameter(Position=2,Mandatory=$true,HelpMessage="Only valid parameters are Local,Dev,Test and Prod")]
      [Alias("env")]
      [ValidateSet("DEV","QA","PROD","NA","PRODG1","PRODG2","PRODG3","PRODG4","QOL","INT","SIT","TEST","UAT")]
      [string]$Environment,
      
      [Parameter(Position=3, HelpMessage="DeployBizTalkMgmtDB e.g. true if its last server to deploy or false otherwise, optional by default true")]
      [Alias("lastserver")]
      [bool]$deployToMgmtDb=$true,
      
      [Parameter(Position=4, HelpMessage="Skip Undeploy e.g. false if undeploy is required before deploy otherwise true, optional by default true")]
      [bool]$skipUndeploy=$true,

	   [Parameter(Position=5,HelpMessage="Optionally, Proivde name of btdf deployment.proj file")]
	   [string]$deploymentProjFileName
      )
      
    $ErrorActionPreference = "Stop"
    
    Write-Host "Installing msi..." -ForegroundColor DarkGray;
     
    InstallMSI $MsiFile $ApplicationInstallPath
 
    Write-Host "Waiting 30 seconds before deploying the app" -ForegroundColor DarkGray;
    start-sleep -seconds 30
    
    Write-Host "Deploying assemblies..." -ForegroundColor DarkGray;
    DeployApplication $ApplicationInstallPath $deployToMgmtDb $Environment $skipUndeploy $deploymentProjFileName
        

    Write-Host "Finished installing and deploying" -ForegroundColor Green;   

}

function UndeployAndUnInstallBizTalkApp
{
       param(
      [Parameter(Position=0,Mandatory=$true,HelpMessage="Msi file should be existing")]
      [Alias("msi")]
      [string]$MsiFile,
             
      [Parameter(Position=1,HelpMessage="Path wherein the app is installed")]
      [ValidateScript({Test-Path $_})]
      [Alias("path")]
      [string]$ApplicationInstallPath,
      
      [Parameter(Position=2,HelpMessage="DeployBizTalkMgmtDB e.g. true if its first server to undeploy or false otherwise, optional by default true")]
      [Alias('firstserver')]
      [bool]$deployToMgmtDb = $true,
	  
	  [Parameter(Position=3,HelpMessage="Optionally, Proivde name of btdf deployment.proj file")]
	  [string]$deploymentProjFileName
      ) 
      
      $ErrorActionPreference = "Stop"
    
      Write-Host "undeploy assemblies..." -ForegroundColor DarkGray;  
      
      UndeployApplication $ApplicationInstallPath $deployToMgmtDb $deploymentProjFileName
      
      Write-Host "uninstalling msi..." -ForegroundColor DarkGray;  
      
      UninstallMSI $MsiFile
      
      Write-Host "Finished uninstalling and undeploying" -ForegroundColor Green;   
      
}

function InstallPipelineComponent
{
    param
    (
        [Parameter(Position=0, Mandatory=$true, HelpMessage="Pipeline component not valid")]
        [Alias('componentname')]
        [string]$PipelineComponent,
        
        [Parameter(Position=1, Mandatory=$true, HelpMessage="Souce directory of pipeline component not valid")]
        [ValidateScript({Test-Path $_})]
        [Alias('source')]
        [string]$SourcePath
    )

    $destination = join-path $BizTalkPath "Pipeline Components\$PipelineComponent"
    $source = join-path $SourcePath $PipelineComponent
    
    Copy -path $source -destination $destination
    GacAssembly $source

}