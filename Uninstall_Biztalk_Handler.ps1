﻿."$PSScriptRoot\BizTalkFunctions\BizTalkFunctions.ps1"
."$PSScriptRoot\BizTalkFunctions\StopApplication.ps1"


function uninstallBiztalkHandler ([Parameter(Mandatory=$true)] [string] $target_env,
                                  [Parameter(Mandatory=$true)] [string] $app_version) {


  $Time = [System.Diagnostics.Stopwatch]::StartNew()
  $ErrorActionPreference = "Stop"
  $filePath = $PSScriptRoot+ "\configs\" + $target_env +"_biztalk_deployment.txt"

  Write-Host "List of environment variables "
  
    Get-Content $filePath | ForEach-Object {
        $var = $_.IndexOf('=')
        $key = $_.SubString(0, $var)
        $value = $_.SubString($var + 1)

        New-Variable -Name $key -Value $value
        Write-Host ($key, $value)
    }

Write-Host "====================================================================================="


Write-Host "Stopping all hosts associated with application" -ForegroundColor Green;

# This might be a better way to manage installed biztalk hosts 
$installedBiztalkHosts = Get-Service -DisplayName "BizTalk Service BizTalk Group :*"

#Stop all biztalk hosts





foreach ($installedBiztalkHost in $installedBiztalkHosts) {

  Write-Host $installedBiztalkHost.DisplayName

  Stop-Service -DisplayName $installedBiztalkHost.DisplayName

  do { $svcstate = Get-Service -DisplayName $installedBiztalkHost.DisplayName | select -expa status; $svcstate } Until ($svcstate -ne "Running") 
  
}


$tempAppName="Connect.Profile.Handler"

$tempAppVersion="0.3.0.21"

$msiName=$AppName +"-"+ $app_version + ".msi"
#build up msi location
$msiLocation = "C:\Octopus\Applications\as-connect-api-biztalk-develop\"+ $AppName +"\" + $app_version +"\content\" + $msiName


#verify msi exists 

if(![System.IO.File]::Exists($msiLocation)){
    throw 'Msi does not exist'
}

$path =  "C:\BizTalkApplications\" +$AppName + "\" + $app_version

New-Item -ItemType Directory -Force -Path $path

stopApplication -AppName $AppName -SQLInstance $SQlInstance -BizTalkManagementDb $BizTalkManagementDb

UndeployAndUnInstallBizTalkApp -msi $msiName -path $path -firstserver $true 


#Restart all biztalk hosts
foreach ($installedBiztalkHost in $installedBiztalkHosts) {

  Write-host "Restarting service " + $installedBiztalkHost.DisplayName

  Start-Service -DisplayName $installedBiztalkHost.DisplayName

  do { $svcstate = Get-Service -DisplayName $installedBiztalkHost.DisplayName | select -expa status; $svcstate } Until ($svcstate -eq "Running")
  
}





Write-Host "Unstallation and Deployment complete " + $Appname +  "Application." -ForegroundColor Green;
  
}


uninstallBiztalkHandler